# Inhouse Infra CICD

Author: Ilias Loukopoulos

This repository contains the setup for a Continuous Integration/Continuous Deployment (CI/CD) pipeline for deploying a Python application using GitLab CI/CD, Docker, Vagrant, Ansible, HAproxy and Kubernetes.

## Overview

In this repository, we utilize a structured folder arrangement to manage different aspects of our project:

- **src/**: Contains the Python application code.
- **devops/**: Includes files related to setting up the CI/CD pipeline.

### CI/CD Pipeline

The CI/CD pipeline consists of four jobs:

1. **Smoke-Tests-Infra**: Basic smoke tests to validate the infrastructure readiness.
2. **Unit-Tests-App**: Runs unit tests for the Python application.
3. **Build**: Builds the Python application Docker image using `devops/Dockerfile`.
4. **Deploy**: Deploys the application in a kind Kubernetes cluster using the built Docker image.

The GitLab Runner utilizes a custom image based on Alpine Linux, including Ansible, Helm, kubectl, kind, Docker, and other minor dependencies for managing the CI/CD pipeline.

## Infrastructure Setup

The on-premises CI/CD infrastructure comprises several key components designed to ensure secure, efficient, and reliable application deployment and monitoring:

- **HAProxy**: Acts as a forward proxy, directing outbound requests from the CI/CD pipeline to various on-premises services. This ensures secure and efficient communication within our infrastructure.
- **Kubernetes (kind)**: A Kubernetes cluster, created using kind (Kubernetes IN Docker), is utilized for deploying applications. The cluster configuration and deployment processes are managed via a Helm chart located in the **devops/** folder.
- **GitLab Runner**: Responsible for managing the processes that run on our infrastructure through the CI/CD pipeline. It's configured to handle various tasks, including testing, building, and deploying applications.
- **Prometheus Blackbox Exporter**: Implements probes to monitor our applications and forwards the metrics to our Prometheus server for analysis. This helps in ensuring application availability and performance.
- **Prometheus Alert Manager**: Configured to send alerts to a test mail server, informing about the uptime of the application and any potential issues detected by monitoring.
- **Prometheus Server**: Collects and stores metrics from various sources within our infrastructure, providing a central point for metrics aggregation and analysis.
- **Grafana Server**: Utilized for creating and managing dashboards that visualize the uptime metrics and other performance indicators collected from our applications and infrastructure.
- **Mailhog**: A test mail server that provides a user interface for viewing the mail alerts sent by Prometheus Alert Manager. This is crucial for testing and validating alert configurations and ensuring that notification workflows are functioning correctly.

## Usage

To successfully deploy and manage your Python application using our CI/CD pipeline, follow these steps:

1. **Deploy the Application**: Execute the command `git push -f origin HEAD:deploy-python-app` to trigger the deployment process of the python application through our CI/CD pipeline.
2. **Access the Application**: Once deployment is complete, access the python application via [http://ropoperperithras.zapto.org:6180/python-app](http://ropoperperithras.zapto.org:6180/python-app) to verify it is running as expected.
3. **Monitor Application Metrics**: Check Prometheus for application metrics at [http://ropoperperithras.zapto.org:9090/](http://ropoperperithras.zapto.org:9090/). This allows you to monitor the application's performance and health.
4. **View Mail Alerts**: Inspect mail alerts sent by Prometheus Alert Manager regarding application uptime and alerts at [http://ropoperperithras.zapto.org:8025/](http://ropoperperithras.zapto.org:8025/) using Mailhog.
5. **Grafana Dashboard**: For a more detailed view of the application's uptime metrics, visit the Grafana dashboard at [http://ropoperperithras.zapto.org:3000/d/bdh0w9t32v6dcf/application-uptime-metrics](http://ropoperperithras.zapto.org:3000/d/bdh0w9t32v6dcf/application-uptime-metrics).
6. **Manage Application with Helm**: To update or manage the application's deployment settings, edit the Helm chart located in the **devops/** folder. This includes configurations like replica counts, resource limits, and more, allowing for fine-tuned management of the application deployment.

Following these steps will guide you through deploying, accessing, monitoring, and managing your Python application within our CI/CD infrastructure.

## References

- Python app repository: [https://github.com/benc-uk/python-demoapp/tree/master](https://github.com/benc-uk/python-demoapp/tree/master)
- Ubuntu Linux distribution
- Vagrant
- Ansible
- Docker images of Kind, HAproxy, Gitlab-Runner.
